console.log ("*** juego adivina quien soy");

let personajes = [{
    name:['UCHIHA','ITACHI'],
    foto:'itachi.jpg',
    preguntas:['Portador del Mangekyo Sharingan ?',' Su especialidad es el ninjutsu ?','Portador del Rinnegan?','Es del clan Uzumaki ?','Domina el elemento Rayo ?'],
    respuestas:['si','no','no','no','no']
},{
    name:['PAIN'],
    foto:'pain.jpg',
    preguntas:['Es del Clan Uchiha ?',' Es del pais de la Lluvia ?','Portador del Rinnegan ?','Su especialidad es el Taijutsu ?','Fue aprendiz de Jiraya ?'],
    respuestas:['no','si','si','no','si']
},{
    name:['KISAME'],
    foto:'kisame.jpg',
    preguntas:['Le dicen el biju sin cola ?','Es desertor de Konoha ?','Fue un ninja de la Niebla ?','Da la vida por sus compañeros ?','Su espada se llama Samaheda ?'],
    respuestas:['si','no','si','no','si']
},{
    name:['OBITO','TOBI'],
    foto:'obito.jpg',
    preguntas:['Es desertor de la aldea de la Roca ?','Utiliza el Taijutsu ?','Fue aprendiz de Kakashi ?','Es del Clan Uchiha ?','Portador del Mangekyo Sharingan ?'],
    respuestas:['no','no','no','si','si']
},{
    name:['HIDAN'],
    foto:'hidan.jpg',
    preguntas:['Es del Clan Hyuga ?','Portador del Sharingan ?','Es creyente de la religion de Jashin ?','Domina bastante el Genjutsu ?','Utiliza Espada ?'],
    respuestas:['no','no','si','no','si']
}
];

const imgPersonaje = document.getElementById("imgPersonaje");
const imgResultado = document.getElementById("imgResultado");
const playJuego =  document.getElementById("playJuego");
const Siguiente =  document.getElementById("Siguiente");
const mostrarPuntaje = document.getElementById('puntaje');
let indice = 1;
let opacidad = 20;
let puntaje = 0;




playJuego.addEventListener('click', () =>{
     

    const pregunta0 = document.getElementById("pregunta0");
    const pregunta1 = document.getElementById("pregunta1");
    const pregunta2 = document.getElementById("pregunta2");
    const pregunta3 = document.getElementById("pregunta3");
    const pregunta4 = document.getElementById("pregunta4");


    imgPersonaje.src = "./assets/pic/" + personajes[indice].foto;
    imgPersonaje.style.filter = "blur(20px)";
    
        pregunta0.value=personajes[indice].preguntas[0];
        pregunta1.value=personajes[indice].preguntas[1];
        pregunta2.value=personajes[indice].preguntas[2];
        pregunta3.value=personajes[indice].preguntas[3];
        pregunta4.value=personajes[indice].preguntas[4];



});


const respuesta0 = document.getElementById("respuesta0")

respuesta0.addEventListener('change', ()=>{
  
     //console.log (" rta " + respuesta0.value)
     

     if (respuesta0.value == personajes[indice].respuestas[0]) {
         opacidad = opacidad - 2
         imgPersonaje.style.filter = "blur(" + opacidad + "px)";
         document.getElementById("icoRta0").src ="./assets/pic/si.png";
         
     } else  {
        document.getElementById("icoRta0").src ="./assets/pic/no.png";
     }

      respuesta0.disabled = true;
});


const respuesta1 = document.getElementById("respuesta1")

respuesta1.addEventListener('change', ()=>{
  
     //console.log (" rta " + respuesta0.value)
     

     if (respuesta1.value == personajes[indice].respuestas[1]) {
         opacidad = opacidad - 2
         imgPersonaje.style.filter = "blur(" + opacidad + "px)";
         document.getElementById("icoRta1").src ="./assets/pic/si.png";
         
     } else  {
        document.getElementById("icoRta1").src ="./assets/pic/no.png";
     }

      respuesta1.disabled = true;
});


const respuesta2 = document.getElementById("respuesta2")

respuesta2.addEventListener('change', ()=>{
  
     //console.log (" rta " + respuesta0.value)
     

     if (respuesta2.value == personajes[indice].respuestas[2]) {
         opacidad = opacidad - 2
         imgPersonaje.style.filter = "blur(" + opacidad + "px)";
         document.getElementById("icoRta2").src ="./assets/pic/si.png";
         
     } else  {
        document.getElementById("icoRta2").src ="./assets/pic/no.png";
     }

      respuesta2.disabled = true;
});

const respuesta3 = document.getElementById("respuesta3")

respuesta3.addEventListener('change', ()=>{
  
     //console.log (" rta " + respuesta0.value)
     

     if (respuesta3.value == personajes[indice].respuestas[3]) {
         opacidad = opacidad - 2
         imgPersonaje.style.filter = "blur(" + opacidad + "px)";
         document.getElementById("icoRta3").src ="./assets/pic/si.png";
         
     } else  {
        document.getElementById("icoRta3").src ="./assets/pic/no.png";
     }

      respuesta3.disabled = true;
});


const respuesta4 = document.getElementById("respuesta4")

respuesta4.addEventListener('change', ()=>{
  
     //console.log (" rta " + respuesta0.value)
     

     if (respuesta4.value == personajes[indice].respuestas[4]) {
         opacidad = opacidad - 2
         imgPersonaje.style.filter = "blur(" + opacidad + "px)";
         document.getElementById("icoRta4").src ="./assets/pic/si.png";
         
     } else  {
        document.getElementById("icoRta4").src ="./assets/pic/no.png";
     }

      respuesta4.disabled = true;
});


const btnRespuesta = document.getElementById("btnRespuesta")

btnRespuesta.addEventListener('click' , () => {
  
    const respuetaGeneral = document.getElementById("respuetaGeneral").value;

      console.log ("respueta fina " + respuetaGeneral)
      let i = 0;
       while(i < personajes[indice].name.length){ 
      if(respuetaGeneral == personajes[indice].name[i]) {
           console.log (" ganaste ") ;
           opacidad = 0;
           puntaje = puntaje + 10;
           imgPersonaje.style.filter = "blur(" + opacidad + "px)";
           imgResultado.src = "./assets/pic/ganaste.png";
           mostrarPuntaje.innerHTML = "Obtuviste " + puntaje + " puntos";
           
      }
       else{
        console.log (" perdiste ") ;
        opacidad = opacidad;
        puntaje = puntaje;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        imgResultado.src = "./assets/pic/perdiste.png";
        mostrarPuntaje.innerHTML = "Obtuviste " + puntaje + " puntos";
      }

      i++;
       

    }
    


})

Siguiente.addEventListener('click', () =>{

    if (indice === 0) {
        Siguiente.disabled = true;
    }

    indice = indice + 1;
     
    const pregunta0 = document.getElementById("pregunta0");
    const pregunta1 = document.getElementById("pregunta1");
    const pregunta2 = document.getElementById("pregunta2");
    const pregunta3 = document.getElementById("pregunta3");
    const pregunta4 = document.getElementById("pregunta4");


    imgPersonaje.src = "./assets/pic/" + personajes[indice].foto;
    imgPersonaje.style.filter = "blur(20px)";
    
        pregunta0.value=personajes[indice].preguntas[0];
        pregunta1.value=personajes[indice].preguntas[1];
        pregunta2.value=personajes[indice].preguntas[2];
        pregunta3.value=personajes[indice].preguntas[3];
        pregunta4.value=personajes[indice].preguntas[4];


});
